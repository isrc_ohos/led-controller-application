# led-controller-application

#### 介绍
基于NFC和socket小灯控制Openharmony js应用

#### 软件架构
软件架构说明


#### 使用说明

1.  安装DevEco Studio IDE工具，版本：DevEco Studio 3.0 Beta1
Build Version: 3.0.0.601, built on October 19, 2021
Runtime version: 11.0.9+11-b944.49 amd64
VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
open-source software
Windows 10 10.0
GC: ParNew, ConcurrentMarkSweep
Memory: 1484M
Cores: 8
Registry: external.system.auto.import.disabled=true

2.  下载源码编译hap包安装

#### 参与贡献
1. 项目主要负责人：郑森文、贾振兴
2. 项目主要贡献者：贾振兴
3. 提交代码
4. 意见/修改代码：可以新建 Pull Request


#### 版本迭代与版本许可
1. v0.1.0-alpha
2. 经过Apache License, version 2.0授权许可。

#### 关于我们
中科院软件所（ISCAS）智能软件研究中心（ISRC）的使命是以智能驱动软件发展，以软件支撑智能创新。

ISRC_OHOS致力于OpenHarmonyOS的生态建设，支撑开源软件供应链关键节点。

网站: https://isrc.iscas.ac.cn/

邮箱: isrc_hm@iscas.ac.cn
