/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default {
    data: {
        ismove:0,
        warParam:'x：77，y:60',
        num: ["","","","","","","","","","","","","","",""],
        number:"",
        car: ["","","","","","","","","","","","","","",""],
        ip: ["","","","","","","","","","","","","","",""],
        cartest: "x:45",
        cartest2: "y:45",
        cartest3: "",
        checkedIp:"",
        ltntest: ["","","","","","","","","","","","","","",""],
        car1:"",
        car2:"",
        car3:"",
        car4:"",
        car5:"",
        car6:"",
        car7:"",
        car8:"",
        car9:"",
        car10:"",
        car11:"",
        car12:"",
        car13:"",
        car14:"",
        car15:"",
        changecontent:"",
        isShow:false,
        isCheck:"",
        status:"正常",
        chat:[''],
        content:[''],
        value:'',
        list:'',
        deviceID:'',
        object: {
            "ip_1": "1",
            "ip_2": "2",
            "ip_3": "3",
            "ip_4": "4",
            "ip_5": "5",
            "ip_6": "6",
            "ip_7": "0",
            "ip_8": "0",
            "ip_9": "0",
            "ip_10": "0",
            "ip_11": "0",
            "ip_12": "0",
            "ip_13": "0",
            "ip_14": "0",
            "ip_15": "0",
            "type": [2,2,3,3,4,1,0,0,0,0,0,0,0,0,0],
        },
        engineCar:[''],
        engineCarIp:[''],
        fightCar:[''],
        fightCarIp:[''],
        droneFly:[''],
        droneFlyIp:[''],
        droneTeamFly:['','','','','',''],
        droneTeamFlyIp:[''],
        patrolCar:['','','',''],
        patrolCarIp:['','','','']
    },


    onCreate() {
        console.info("Application onCreate");
    },
    onDestroy() {
        console.info("Application onDestroy");
    }
};
