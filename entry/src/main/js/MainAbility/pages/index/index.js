import prompt from '@system.prompt';
/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import systemTime from '@ohos.systemTime';

import led_controller from '@ohos.led_controller';


//var netAddress = {
//    address: '192.168.1.127',
//    port: 8888
//}
//var tcpConnectionOptions = {
//    netAddress: netAddress,
//    timeout: 6000
//}
var ConnectionStatus = 0

export default {
    data: {
        title: "",
        ip:"",
        changecontent:""
    },
    onInit() {
    this.tcpConnect();
    },
    onShow(){

    },
   onDestroy(){
       this.tcpDistroy();
   },
    //results.connection_status
    tcpConnect(){
        console.log("[led Controller] Function tcpConnect called")
        let promise_connect = led_controller.Connect();
        console.log("[led Controller] car_controller.Connect called, waiting for promise...")
        promise_connect.then((results)=>{
            console.log('[led Controller] promise resolved')

                ConnectionStatus = 1
                console.log('[led Controller] connect success')
                prompt.showToast({
                    message: "配网成功",
                });
        }).catch(err =>{
            console.log('[led Controller]'+err)
        })
        console.log("[led Controller] Function tcpConnect end")
    },
    tcpSend(message){
        let promise_send = led_controller.Send({data:message})
        promise_send.then((results) => {
            console.log("[Car Controller] promise resolved")
            if (results.send_status == 1){
                console.log("[Car Controller] send success")

            }
            else {
                console.log("[Car Controller] send fail")
            }
        }).catch(err=>{
            console.log("[Car Controller]"+err)
        })
    },
    tcpDistroy(){
        let promise_disconnect = led_controller.Close()
        promise_disconnect.then((results)=>{
            console.log("[led Controller] promise resolved")
            if (results.close_status == 1){
                console.log("[led Controller] close success")
                ConnectionStatus = 0
                prompt.showToast({
                    message: "网络断开",
                });
            }
            else {
                console.log("[led Controller] close fail")
            }
        }).catch(err=>{
            console.log("[Car Controller]"+err)
        })

    },
    ledOpen(){
        console.log("led open")
        console.log("[Car Controller] Function onStopPressed called")
        this.tcpSend("1")
},

    ledClose(){
        console.log("led close")
        console.log("[led Controller] Function ledClose called")
        this.tcpSend("0")
    },
    onDisConnectPressed(){
        console.log("[Car Controller] Function onDisConnectPressed called")
        if (ConnectionStatus == 1){
           // this.tcpSend("断开链接")
            this.tcpDistroy()
        }
        else {
            console.log("[Car Controller] Error! Tcp not Connect")
        }
    }
}



